package com.crud.demo.JWT.Utility;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.crud.demo.JWT.Entitys.ERole;
import com.crud.demo.JWT.Entitys.Role;
import com.crud.demo.JWT.Repositorys.RoleRepository;




@Component
public class DatosIniciales implements CommandLineRunner {
  


    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {

        if (roleRepository.findAll().size() == 0) {
            Role rolAdmin = new Role(ERole.ROLE_ADMIN);
            Role rolUser = new Role(ERole.ROLE_USER);
            Role rolCahser = new Role(ERole.ROLE_MODERATOR);
            roleRepository.save(rolAdmin);
            roleRepository.save(rolUser);
            roleRepository.save(rolCahser);
        }
       

    }
    
}
