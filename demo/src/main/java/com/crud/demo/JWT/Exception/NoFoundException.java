package com.crud.demo.JWT.Exception;

public class NoFoundException extends RuntimeException {
    private static final long serialVesrionUID = 1l;

    public NoFoundException(String message) {
        super(message);
    }   
}
