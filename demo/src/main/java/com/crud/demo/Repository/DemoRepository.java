package com.crud.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.crud.demo.Entity.Demo;

@Repository
public interface DemoRepository extends CrudRepository<Demo,Long> {
    
}
