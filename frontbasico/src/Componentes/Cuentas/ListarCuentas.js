import axios from "axios";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { APIC } from "../Config/ApiUrl";
import swal from "sweetalert";
import Menu from "../Menu/Menu";

const URL = APIC("list");
const URLE = APIC("delete/");
const ListarCuentas = () => {
  const [cuentas, setCuentas] = useState([]);

  useEffect(() => {
    traerCuentas();
  }, []);
  const traerCuentas = async () => {
    try {
      const cuentas = await axios({
        method: "GET",
        url: URL,
      });
      setCuentas(cuentas.data);
    } catch (error) {}
  };

  const eliminarCuenta = async (id) => {
    swal({
      title: "Eliminar Registro",
      text: "Esta seguro de eliminar el registro",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(async (willDelete) => {
      if (willDelete) {
        try {
          const eliminar = await axios({
            method: "DELETE",
            url: URLE + id,
          });

          swal("Registro elimindo", eliminar.data.message, "success");
          traerCuentas();
        } catch (error) {
          swal("Acceso No Autorizado", JSON.parse(error.request), "error");
        }
      } else {
        swal("El registro no se borró");
      }
    });
  };
  return (
    <>
      <Menu name="cuentas" />
      <div className="container">
        <Link className="btn btn-outline-primary" to={`/crearCuenta`}>
        <i className="fa-solid fa-sack-dollar"></i>
        </Link>
        <table className="table">
          <thead className="table-primary">
            <tr>
              <th>Fecha</th>
              <th>Saldo</th>
              <th>Cliente</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {cuentas.map((cuenta) => (
              <tr key={cuenta.id}>
                <td>{cuenta.fechaApertura.substring(0, 10)}</td>
                <td>{cuenta.saldoCuenta}</td>
                <td>
                  {cuenta.cliente.nombreCliente +
                    " " +
                    cuenta.cliente.apellidoCliente}
                </td>
                <td>
                  {" "}
                  <Link
                    className="btn btn-outline-danger"
                    onClick={() => eliminarCuenta(cuenta.id)}
                  >
                    <i className="fa-solid fa-trash-can"></i>
                  </Link>{" "}
                  <Link
                    className="btn btn-outline-info"
                    to={`/editarCuenta/${cuenta.id}`}
                  >
                    <i className="fa-solid fa-user-pen"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ListarCuentas;
