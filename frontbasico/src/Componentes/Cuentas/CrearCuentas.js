import Menu from "../Menu/Menu";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { APIC, API } from "../Config/ApiUrl";
import swal from "sweetalert";
import axios from "axios";
const URL = APIC("create");
const URLC = API("list");

const headers = {
  user: sessionStorage.getItem("user"),
  key: sessionStorage.getItem("key"),
};
const CrearCuenta = () => {
  const [fecha, setFecha] = useState("");
  const [saldo, setSaldo] = useState(0);
  const [cliente, setCliente] = useState("");
  const [clientes, setClientes] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    traerClientes();
  }, []);

  const crear = async () => {
    if (cliente === "") {
      swal("Cuentas", "Debe seleccionar un cliente", "error");
    } else {
      try {
        const crearCuenta = await axios({
          method: "POST",
          url: URL,
          data: {
            fechaApertura: fecha,
            saldoCuenta: saldo,
            cliente: {
              idCliente: cliente,
            },
          },
        });

        swal(
          "Cuentas",
          "Cuenta Creada con id " + crearCuenta.data.id,
          "success"
        ).then((value) => {
          navigate("/cuentas");
        });
      } catch (error) {
        swal("Cuentas", "Error al crear la cuenta", "error");
      }
    }
  };

  const traerClientes = async () => {
    try {
      const clientes = await axios({
        method: "GET",
        url: URLC,
        headers: headers,
      });
      setClientes(clientes.data);
    } catch (error) {}
  };
  return (
    <>
      <Menu />
      <div className="container col-5">
        <h3>Creando cuenta</h3>
        <form onSubmit={crear}>
          <div>
            <label className="form-label">Fecha</label>
            <input
              className="form-control"
              value={fecha}
              onChange={(e) => setFecha(e.target.value)}
              type="date"
              required
              onInvalid={(e) =>
                e.target.setCustomValidity("El campo fecha es requerido")
              }
              onInput={(e) => e.target.setCustomValidity("")}
            ></input>
          </div>
          <div>
            <label className="form-label">Saldo</label>
            <input
              className="form-control"
              value={saldo}
              onChange={(e) => setSaldo(e.target.value)}
              type="number"
              min={0}
            ></input>
          </div>

          <div>
            <label className="form-label">Cliente</label>
            <select
              className="form-control"
              value={cliente}
              type="text"
              onChange={(e) => setCliente(e.target.value)}
              required
            >
              <option>Seleccione un cliente</option>
              {clientes.map((cliente) => (
                <option value={cliente.idCliente} key={cliente.idCliente}>
                  {cliente.nombreCliente + " " + cliente.apellidoCliente}
                </option>
              ))}
            </select>
          </div>
          <button type="submit" className="btn btn-outline-primay">
            Guardar
          </button>
        </form>
      </div>
    </>
  );
};

export default CrearCuenta;
