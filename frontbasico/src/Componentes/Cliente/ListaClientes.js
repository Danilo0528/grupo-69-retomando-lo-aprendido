/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { API } from "../Config/ApiUrl";
import swal from "sweetalert";
import Menu from "../Menu/Menu";

const URL = API("list");
const URLE = API("delete/");

const ListarClientes = () => {
  const [Cliente, setClientes] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    try {
      let tipoUsuario = "user";
      if (sessionStorage.getItem("key") === null) {
        swal("Acceso No Autorizado", "Debe digitar credenciales", "error");
        navigate("/");
      }
      JSON.parse(sessionStorage.getItem("roles")).forEach((element) => {
        if (element.nombre === "ROLE_ADMIN") {
          tipoUsuario = "admin";
        }
      });
      if (tipoUsuario === "user") {
        swal(
          "Acceso No Autorizado para cliente",
          "Debe digitar credenciales",
          "error"
        );
        navigate("/menu");
      } else {
        getClientes();
      }
    } catch (error) {}
  }, []);

  const getClientes = async () => {
    try {
      const login = await axios({
        method: "GET",
        url: URL,
        headers: {
          user: sessionStorage.getItem("user"),
          key: sessionStorage.getItem("key"),
        },
      });
      setClientes(login.data);
    } catch (error) {
      if (error.response.request.status === 401) {
        swal(
          "Acceso No Autorizado",
          "El usuario no tiene permiso de acceso",
          "error"
        );
        navigate("/menu");
      }
    }
  };

  const eliminarCliente = async (id) => {
    swal({
      title: "Eliminar Registro",
      text: "Esta seguro de eliminar el registro",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(async (willDelete) => {
      if (willDelete) {
        try {
          const eliminar = await axios({
            method: "DELETE",
            url: URLE + id,
          });

          swal("Registro elimindo", eliminar.data.message, "success");
          getClientes();
        } catch (error) {
          swal("Acceso No Autorizado", JSON.parse(error.request), "error");
        }
      } else {
        swal("El registro no se borró");
      }
    });
  };

  return (
    <>
      <Menu name="Clientes" />
      <div className="container">
        <Link className="btn btn-outline-primary" to={`/crearCliente`}>
          <i className="fa-solid fa-user-plus"></i>
        </Link>
        <table className="table">
          <thead className="table-primary">
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>usuario</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {Cliente.map((cliente) => (
              <tr key={cliente.idCliente}>
                <td>{cliente.nombreCliente}</td>
                <td>{cliente.apellidoCliente}</td>
                <td>{cliente.userName}</td>
                <td>
                  <Link
                    className="btn btn-outline-danger"
                    onClick={() => eliminarCliente(cliente.idCliente)}
                  >
                    <i className="fa-solid fa-trash-can"></i>
                  </Link>{" "}
                  <Link
                    className="btn btn-outline-info"
                    to={`/editarCliente/${cliente.idCliente}`}
                  >
                    <i className="fa-solid fa-user-pen"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ListarClientes;
