/* eslint-disable react-hooks/exhaustive-deps */
import Menu from "../Menu/Menu";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { API } from "../Config/ApiUrl";
import swal from "sweetalert";
import axios from "axios";
const URL = API("create") ;
const CrearCliente = () => {
  const navigate = useNavigate();
  const [nombreCliente, setnombreCliente] = useState("");
  const [apellidoCliente, setapellidoCliente] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [regresar, setRegresar] = useState(0);
  

  const headers = {
    user: sessionStorage.getItem("user"),
    key: sessionStorage.getItem("key"),
  };

  useEffect(() => {
    if (sessionStorage.getItem("key") === null) {
      swal("Acceso No Autorizado", "Debe digitar credenciales", "error");
      navigate("/");
    }
    
  }, []);

  useEffect(() => {
    if(regresar!==0)
    navigate("/clientes");
  }, [regresar]);

  const guardar = async () => {
    let roles = document.getElementsByClassName("role");
    let listaRoles = [];
    for (const rol of roles) {
      console.log(rol);
      if (rol.checked) {
        listaRoles.push(rol.value);
      }
    }
    try {
      const insertarCliente = await axios({
        method: "POST",
        url: URL,
        data: {
          nombreCliente: nombreCliente,
          apellidoCliente: apellidoCliente,
          userName: userName,
          password: password,
          roles: listaRoles,
        },
        headers: headers,
      });
      console.log(insertarCliente.data);
      swal("Registro creado", insertarCliente.data.message, "success").then(
        (value) => {
         setRegresar(1)
        }
      );
    } catch (error) {
      swal(
        "Error en datos",
        JSON.parse(error.request.response).errors[0].message,
        "error"
      );
    }
    console.log(listaRoles);
  };

  return (
    <>
      <Menu></Menu>
      <div className="container col-5">
        <form onSubmit={guardar}>
          <div className="mb-3">
            <label className="form-label">Nombre de cliente </label>{" "}
            <input
              className="form-control"
              type="text"
              value={nombreCliente}
              onChange={(e) => setnombreCliente(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <label className="form-label">Apellido de cliente </label>{" "}
            <input
              className="form-control"
              type="text"
              value={apellidoCliente}
              onChange={(e) => setapellidoCliente(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <label className="form-label">username de cliente </label>{" "}
            <input
              className="form-control"
              type="text"
              value={userName}
              onChange={(e) => setUserName(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <label className="form-label">Contraseña de cliente </label>{" "}
            <input
              className="form-control"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <input
              className="form-check-input role"
              type="checkbox"
              value="user"
            />{" "}
            <label className="form-check-label">User</label>
            {"   "}
            <input
              className="form-check-input role"
              type="checkbox"
              value="admin"
            />{" "}
            <label className="form-check-label">Admin</label>
            {"   "}
            <input
              className="form-check-input role"
              type="checkbox"
              value="cash"
            />{" "}
            <label className="form-check-label">Casher</label>
          </div>

          <button type="submit" className="btn btn-outline-primary">
            Guardar
          </button>
          {" "}
          <Link className="btn btn-outline-primary" to="/clientes">Regresar</Link>
        </form>
      </div>
    </>
  );
};

export default CrearCliente;
