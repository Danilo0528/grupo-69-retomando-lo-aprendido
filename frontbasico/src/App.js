/* eslint-disable jsx-a11y/alt-text */
import "./App.css";
import {Route,Routes,HashRouter} from "react-router-dom"
import Login from "./Componentes/Login/Login";
import Menu from "./Componentes/Menu/Menu";
import ListarClientes from "./Componentes/Cliente/ListaClientes";
import CrearCliente from "./Componentes/Cliente/CrearCliente";
import EditarCliente from "./Componentes/Cliente/EditarCliente";
import ListarCuentas from "./Componentes/Cuentas/ListarCuentas";
import EditarCuentas from "./Componentes/Cuentas/EditarCuentas";
import CrearCuenta from "./Componentes/Cuentas/CrearCuentas";



function App() {

  return (
    <>
      <HashRouter>
        <Routes>
          <Route exact path="/" element={<Login/>}/>
          
          <Route exact path="/menu" element={<Menu/>}/>          
          <Route exact path="/clientes" element={<ListarClientes/>}/>          
          <Route exact path="/crearCliente" element={<CrearCliente/>}/>          
          <Route exact path="/editarCliente/:id" element={<EditarCliente/>}/>  
          <Route exact path="/cuentas" element={<ListarCuentas/>}/>
          <Route exact path="/editarCuenta/:id" element={<EditarCuentas/>}/>  
          <Route exact path="/crearCuenta" element={<CrearCuenta/>}/>          
        </Routes>

      </HashRouter>
    </>
  );
}

export default App;
