/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useState,useEffect } from "react";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import { APITEST } from "./Config";

const API = APITEST("user")
const User = () => {
    let navigate = useNavigate()
    const [contenido, setContenido] = useState("");
    useEffect(() => {
        getContenido()
    }, []);
    const getContenido = async ()=>{
        try {
            const admin = await axios({
                method:"GET",
                url:API,
                headers:{
                    Authorization:"Bearer "+sessionStorage.getItem("token")
                }
            })
            setContenido(admin.data)
        } catch (error) {
            swal("Acceso no Autorizado", "Digite credenciales de usuario", "error").then(
                (value) => {
                  navigate("/login");
                }
              );
        }
    }

    return ( <>{contenido}</> );
}
 
export default User;