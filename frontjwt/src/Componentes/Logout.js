/* eslint-disable react-hooks/exhaustive-deps */
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import swal from "sweetalert";
const Logout = () => {
  let navigate = useNavigate();
  sessionStorage.clear();
  useEffect(() => {
    swal("Cerrando Sesión", "Sesión cerrada", "success").then(
      (value) => {
        navigate("/login");
      }
    );
  }, []);
  return <></>;
};

export default Logout;
