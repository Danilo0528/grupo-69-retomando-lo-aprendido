import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import swal from "sweetalert";
const Menu = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/login">
            Login
          </Link>

          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <Link className="navbar-brand " to="/logout">
              Logout
            </Link>
            <Link className="navbar-brand " to="/admin">
              Administrador
            </Link>
            <Link className="navbar-brand " to="/user">
              usuario
            </Link>
            <Link className="navbar-brand " to="/mod">
              moderador
            </Link>
            <Link className="navbar-brand " to="/all">
              publico
            </Link>
            <Link className="navbar-brand " to="/todos">
              Cualquier usuario con credenciales
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Menu;
