import logo from './logo.svg';
import './App.css';
import {Route,Routes,HashRouter} from "react-router-dom"
import Menu from './Componentes/Menu';
import Login from './Componentes/Login';
import Logout from './Componentes/Logout';
import Admin from './Componentes/ContenidoAdministrador';
import User from './Componentes/ContenidoUsuario';
import Modedador from './Componentes/ContenidoModerador';
import Public from './Componentes/ContenidoPublico';
import Todos from './Componentes/ContenidoTodos';
function App() {
  return (
    <div className="App">
      <HashRouter>
      <Menu></Menu>
        <Routes>
          <Route exact path="/login" element={<Login/>}/>  
          <Route exact path="/logout" element={<Logout/>}/>          
          <Route exact path="/admin" element={<Admin/>}/>          
          <Route exact path="/user" element={<User/>}/>  
          <Route exact path="/mod" element={<Modedador/>}/>
          <Route exact path="/all" element={<Public/>}/>          
          <Route exact path="/todos" element={<Todos/>}/>          
        </Routes>

      </HashRouter>
    </div>
  );
}

export default App;
