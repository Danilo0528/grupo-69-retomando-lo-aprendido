package com.backend.mongo.Entity;

import java.util.Date;

import lombok.Data;

@Data
public class ConsultaFechaDTO {
    private Date fechaInicial;
    private Date fechaFinal;
}
