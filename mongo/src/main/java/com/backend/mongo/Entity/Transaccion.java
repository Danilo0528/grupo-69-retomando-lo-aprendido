package com.backend.mongo.Entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(value="transacciones")
@Data
public class Transaccion {
    @Id
    private String idTransaccion;
    private Date fechaTransaccion;
    private String tipoTransaccion;
    private double valorTransaccion;
    @DBRef
    private Cuenta cuenta;
}
