package com.backend.mongo.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(value = "Clientes")
@Data
public class Cliente {
    @Id
    private String idCliente;
    private String nombreCliente;
    private String emailCliente;
    private String claveCliente;
}
