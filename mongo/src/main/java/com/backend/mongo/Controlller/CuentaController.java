package com.backend.mongo.Controlller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.mongo.Entity.Cuenta;
import com.backend.mongo.Service.CuentaService;

@RestController
@RequestMapping("/api/v1/cuenta")
public class CuentaController {
    @Autowired
    CuentaService cuentaService;

    @PostMapping("/create")
    public Cuenta save(@RequestBody Cuenta cuenta){
        return cuentaService.save(cuenta);
    }

    @GetMapping("/list")
    public List<Cuenta> findAll(){
        return cuentaService.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable("id")String id){
        return cuentaService.deleteById(id);
    }

    @PutMapping("/update/{id}")
    public Cuenta save(@RequestBody Cuenta cuenta,@PathVariable("id")String id){
        cuenta.setIdCuenta(id);
        return cuentaService.save(cuenta);
    }

    @GetMapping("/list/{id}")
    public Cuenta findAll(@PathVariable("id")String id){
        return cuentaService.findById(id);
    }

    
    @GetMapping("/consulta/{id}")
    public List<Cuenta> buscarPorCliente(@PathVariable("id")String id){
        return cuentaService.findByUser(id);
    }
}
