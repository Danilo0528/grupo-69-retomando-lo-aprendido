package com.backend.mongo.Controlller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.mongo.Entity.ConsultaFechaDTO;
import com.backend.mongo.Entity.Cuenta;
import com.backend.mongo.Entity.Transaccion;
import com.backend.mongo.Service.CuentaService;
import com.backend.mongo.Service.TransaccionService;

@RestController
@RequestMapping("/api/v1/transaccion")
public class TransaccionController {
    @Autowired
    TransaccionService transaccionService;

    @Autowired
    CuentaService cuentaService;

    @PostMapping("/create")
    public ResponseEntity< String> save(@RequestBody Transaccion transaccion) {
        Cuenta cuenta = cuentaService.findById(transaccion.getCuenta().getIdCuenta());

        if(transaccion.getTipoTransaccion().equals("R")){
            if(transaccion.getValorTransaccion()>cuenta.getSaldoCuenta()){
                return new ResponseEntity<>("fondos insuficientes",HttpStatus.BAD_REQUEST);
            }
            cuenta.setSaldoCuenta(cuenta.getSaldoCuenta()-transaccion.getValorTransaccion());
            cuentaService.save(cuenta);
        }else{
            cuenta.setSaldoCuenta(cuenta.getSaldoCuenta()+transaccion.getValorTransaccion());
            cuentaService.save(cuenta);
        }
        transaccionService.save(transaccion);
     
        return new ResponseEntity<>("Transaccion realizada",HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public Transaccion update(@RequestBody Transaccion transaccion, @PathVariable("id") String id) {
        transaccion.setIdTransaccion(id);
        return transaccionService.save(transaccion);
    }

    @GetMapping("/list")
    public List<Transaccion> findAll() {
        return transaccionService.findAll();
    }

    @GetMapping("/list/{id}")
    public Transaccion findById(@PathVariable("id") String id) {
        return transaccionService.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable("id") String id) {
        return transaccionService.deleteById(id);
    }

    @GetMapping("/consultar/{id}")
    public List<Transaccion> findCuenta(@PathVariable("id") String id) {
        return transaccionService.buscarCuenta(id);
    }

    @PostMapping("/fecha")
    public List<Transaccion> findFecha(@RequestBody ConsultaFechaDTO consultaFechaDTO) {

        return transaccionService.buscarFecha(consultaFechaDTO.getFechaInicial(), consultaFechaDTO.getFechaFinal());
    }
}
