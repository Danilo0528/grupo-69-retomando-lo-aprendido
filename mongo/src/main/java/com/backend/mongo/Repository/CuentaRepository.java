package com.backend.mongo.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.backend.mongo.Entity.Cuenta;

@Repository
public interface CuentaRepository extends MongoRepository<Cuenta,String> {
    
    @Query("{cliente:?0}")
    List<Cuenta> buscarPorCliente(String idCliente);

}
