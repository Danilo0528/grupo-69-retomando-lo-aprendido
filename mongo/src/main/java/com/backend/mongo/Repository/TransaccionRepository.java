package com.backend.mongo.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.backend.mongo.Entity.Transaccion;

@Repository
public interface TransaccionRepository extends MongoRepository<Transaccion,String> {
    @Query("{cuenta:?0}")
    public List<Transaccion> consultarTransaccionesPorCuenta(String id);

    @Query("{fechaTransaccion:{$gte:?0,$lte:?1}} }")
    public List<Transaccion> consultarFecha(Date fInicial,Date fFinal);
}
